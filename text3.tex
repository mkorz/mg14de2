% sample main.tex created 2015-09-21 by bob jantzen
\documentclass{ws-procs975x65}
% optional packages
%\usepackage{graphicx}

\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\newcommand{\bit}{\begin{itemize}}
\newcommand{\eit}{\end{itemize}}
\newcommand{\bfi}{\begin{figure}}
\newcommand{\efi}{\end{figure}}
\newcommand{\bfic}{\begin{figure*}}
\newcommand{\efic}{\end{figure*}}
\newcommand{\bce}{\begin{center}}
\newcommand{\ece}{\end{center}}
\newcommand{\bt}{\begin{table}}
\newcommand{\et}{\end{table}}
\newcommand{\btb}{\begin{tabular}}
\newcommand{\etb}{\end{tabular}}


\begin{document}


\title{Inhomogeneous cosmology and backreaction: prospects for future development}
\author{Krzysztof Bolejko$^1$ and Miko\l{}aj Korzy\'nski$^2$}

\address{$^1$University Department, University Name, City, State ZIP/Zone, Country\\
$^2$Center for Theoretical Physics, Polish Academy of Sciences, Al. Lotnik\'ow 32/46, 02-668 Warsaw, Poland}


\begin{abstract}
...
\end{abstract}

\keywords{.;.;}

\bodymatter




\section{Introduction}

In the last decade we have witnessed a significant progess in the field of cosmology. It was fuelled by the increasing amount of observational on one hand and
the theoretical advancements in its interpretation on the other. The data is commonly interpreted within the $\Lambda$CDM framework - the paradigm
of most of modern cosmological research. In this paradigm it is assumed that the large--scale geometry of the Universe is descirbed by the spatially homogeneous and isotropic
FLRW metric plus small metric perturbation, treated mostly at the linear level, describing the influence of the observed matter distribution inhomogeneities. 
The dynamics of the FLRW model is governed by the Friedmann equations with the matter sources assumed to be mostly in the form of dark energy and cold dark matter.
The baryonic component is believed to be very small ($\Omega_B \approx 0.05$).
This set of assumptions is enough to account for most of the cosmological observations, including the inhomogeneities of the cosmic microwave background, the redshift--luminosity
relation for the type Ia supernovae. 

However, a number of researcher  questioned various assumptions of this model. Firstly, the assumption of homogeneity
seems to hold only on very large scales (possibly larger than 2000 Mpc \cite{Horvath:2013kwa}, comparable to the Hubble scale). Below that we see a hierarchy of 
structures: underdense regions surrounded by walls and filaments, made of galaxy clusters which in turn are made of galaxies. 
In what sense is thus the FLRW metric a good approximation of the real spacetime? And to what extend should we trust
results based on the perturbations of the background metric, assumed to be FLRW? 

The full, physical metric tensor follows the complicated distribution of matter and therefore contains various perturbations 
on small scales. In order to define the large--scale metric describing it on the Hubble scales we need to remove these ripples
scale after scale. The removal of fine structures and the corresponding degrees of freedom from a complex system is 
known in theoretical physics as \emph{coarse--graining}. It is a fairly standard procedure in statistical physics or quantum field theory,
but in the context of GR it seems to be poorly researched. 

The coarse--graining for a fixed physical metric $g_{\mu\nu}^\textit{phys}$ amounts to defining an idealized, smoothed metric
$g_{\mu\nu}^\textit{av}$, which represents the graviational field without the contribution from the ripples below the
coarse--graining scale. In the same manner we may average the stress--energy tensor $T_{\mu\nu}^\textit{phys}$
obtaining $T_{\mu\nu}^\textit{av}$. Assuming the Einstein equations to hold exaclty for $g_{\mu\nu}^\textit{phys}$: 
\bea
G_{\mu\nu}\left[ g_{\mu\nu}^\textit{phys}\right] = 8\pi G T_{\mu\nu}^\textit{phys}
\eea
we can derive the effective Einstein equations holding on the large scales
\bea
G_{\mu\nu}\left[ g_{\mu\nu}^\textit{av}\right] = 8\pi G \left(T_{\mu\nu}^\textit{av} + B_{\mu\nu}\right).
\eea
The additional term $B_{\mu\nu}$ on the right hand side arises because the Einsteint equations, unlike for example
 the Maxwell's equations of electromagnetism, are nonlinear and thus the average of the local $G_{\mu\nu}$
 is not equal to $G_{\mu\nu}$ of the average metric tensor. $B_{\mu\nu}$ is known as the backreaction \cite{Ellis:2011}. 
 In practice we may need more than one application of the coarse--graining procedure to reach the homogeneity scale
 of the solution. The total backreaction in this case will consist of the sum of contributions from all intermediate scales \cite{Ellis:2011, Korzynski:2014nna}.

 The question of backreaction sparked an intensive debate in the early 2000's [...]. Initially the debate was centered at the issue whether the 
 influence of the inhomogeneities can explain the accelerated expansion of the Universe and thus get rid of
 the dark energy problem \cite{Kolb:2005da, Rasanen:2003fy, Buchert:1999er, Buchert:2001sa}. 
 This idea seems to have died out, but the debate evolved later into the question whether the inhomogeneities of the matter distribution require a fully nonlinear, relativistic 
 approach or whether the perturbative \cite{Baumann:2010tm} or the short wavelength approach is sufficient \cite{Ishibashi:2005sj, Green:2010qy, Green:2014aga, Green:2015bma}. The issue is fairly complex, because on one hand the density contrasts
 encountered on fine scales may be enormous ($\delta \approx 10^{30}$), but on the other hand the metric perturbations they cause do not have to be very large except the vicinity of very compact objects (neutron stars or black holes). The debate seems inconclusive so far,
 for the latest voices see Refs.~\citenum{Buchert:2015iva, Green:2016cwo}.

Other problems are of more practical and observational nature: how should we pick the background FLRW? Obviously it should be 
the one which fits best into the observational data -- but in what sense exactly? This issue was raised by Ellis and Stoeger in 1987 \cite{Ellis:1987zz}.  
It is obviously connected with the question how the metric inhomogeneities
present on small scales affect the cosmological observations. Note that the interpretation of any cosmological data, such as the
SN observations, involves
implicitly the presence of the background FLRW, whose parameters we estimate from those very observations.

The progress in the numerical techniques applied to general relativity \cite{Cardoso:2014uka, Sperhake:2014wpa, Pretorius:2005gq}  opens up a new approach to the problem based on 
the full 3+1 relativistic simulations 
of the Universe. These simulations may correct or at least complement the Newtonian $N$--body simulations of the stucture formation performed in the last decade
 \cite{2009MNRAS.398.1150B, Springel:2005nw}. 
They can also probe the optical properties of the Universe models in detail, thereby helping to estimate the influence of the inhomogeneities on the observables.



\section{Survey of the community}


The number of people actively working in the filed of inhomogeneous cosmology is of order 100 people which is quite small compared to those working within the paradigm of the FLRW models (for example the Planck team counts more than 200 people).

In order to get a better feeling of the community, and find out what researchers working in this field find important and how they think this domain of research will develop in the near future, we designed a short survey. A survey consisted of the following 3 questions:

\begin{enumerate}
\item
What do you think is the most important topic (or range of topics) in cosmology in general?
\item
Within a domain of inhomogeneous cosmology which topic (or topics) are in your opinion the most important?
\item
 How do you think the field of inhomogeneous cosmology will (or should) develop over the next 5--10 years?
\end{enumerate}

We emailed these questions to around 70 people, from which around 50 have responded. We manage to obtain a good representation both in terms of demographics (from very senior and highly regarded in the community people to junior academics) and geography (from every continent except for Antarctica). 

Below we present a short summary of the answers we received. In order to compactify the responses we decided to bin them into specific areas. While this solution is not perfect and some of the bins overlap, such a procedure enabled us to report the responses in a rather compact way. Also, when an answer to our questions was unique and such an opinion expressed by a respondent was not shared by any other person we decided not to include such a response in the analysis below. 
         
\subsection{ Question 1: What do you think is the most important topic (or range of topics) in cosmology in general?}

\begin{figure}[h]
\begin{center}
\caption{{\em What do you think is the most important topic (or range of topics) in cosmology in general?}}
\includegraphics[scale=0.245]{question1.eps}
\label{q1-fig}
\end{center}
\end{figure}

\paragraph{Dark sector:}

Most respondents answered that the most important in modern cosmology is the nature of dark sector and the fundamental physics behind it.

A number of respondents pointed out that we need work on both fronts: experimental, with a direct detection of dark matter and energy, and theoretical, with better theories exploiting properties of dark sector. We need to examine if these phenomena are in some way related to each other.
 
While most people answered that both: dark energy and dark matter, are equally important, some made a clear distinction, claiming that dark matter should take precedent as it leads to synergy between cosmologist and particle physics, and that dark energy being most likely the cosmological constant is not of significant importance. Others though pointed out that dark energy being a cosmological constant would require an improbable amount of fine tuning and is unnatural from the point of view of quantum theory and thus studies of dark energy could open a new window to fundamental physics.

Whether it is a real phenomenon or merely a phenomenological description of either apparent acceleration of the universe or rotation curves of galaxies we need better descriptions and more alternative models --- reducing these phenomena just to two numbers ($\Omega_m$ and $\Omega_\Lambda$) is unacceptable.


\paragraph{Early universe:}

More than a third of respondents pointed out that the initial conditions for the Universe pose one of the most important challenges for modern cosmology. The majority pointed out weakness of inflation, and that we need other alternative theories of conditions and problems related to the early universe.

Apart from initial conditions of the early universe, inflation, re-heating, baryogenesis, baryon asymmetry, and studies of physical processes that took place in the ealry universe were mentioned and it was stress out by many that early universe open a new window to fundamental physics.


\paragraph{Dealing with observations:}

Almost a quarter of respondents pointed out that we are entering era where a very large number of high precision data becomes or will some become available. New measurements are coming online (for example gravitational waves) and soon we will be able to measure directly the expansion of the universe via the redshift drift, probe dark ages and the epoch or reionziation, deep and wide galaxy redshift surveys, lensing surveys this will all lead most certainly to new discoveries and perhaps a paradigm shift.

The new data will also bring new challenges. We will certainly need to have a better understanding of how various physical processes impact observables. We will also need to learn how to deal with such a large amount of data. The usual approach is to compress the data by projecting it only smaller number of functions or parameters. Care needs to be taken in order to perform such reduction in rather model independent manner and not introduce model’s assumptions in to the compressed data and/or to analysis itself.

Thus a lot of effort should be put forward to scrutinise and falsify various aspects of the concordance cosmology.

We also need to be aware of other models predictions in order not to jump in to conclusions too early. It happened often when the data, which seemed to be consistent with model’s prediction, was taken as a prove that the model is true and faithful representation of our Universe. Sometimes this lead to ``discoveries’’ that were are announced prematurely.

Thus, with this new probes and data care should be taken to either to extract information in model independent manner and in addition be aware of predictions of alternative models. Without this any debate on precisions vs accurate cosmology may not be conclusive.



\paragraph{Backreaction and inhomogeneous cosmology:}

Almost a quarter of responses pointed out that the main challenge of modern cosmology is to better understand the effects associated with the inhomogeneous structure of the universe and backreaction. We will elaborate more on this point when discussing Question 2 and 3.


\paragraph{Cosmological probes of fundamental physics and modified gravity:}

Almost a fifth of respondents pointed out that cosmology provides a great opportunity to find new connections between cosmology and fundamental physics. It was noted that cosmological observations can be use to test the laws of nature and its fundamental components --- from the nature of dark matter, dark energy, conditions of the early universe to modified gravity. 

\paragraph{Cosmic topology:}

A very few people argued that one of the important domains of modern cosmology is to find out what is the overall topology of the universe we live in. 





\subsection{Question 2: Within a domain of inhomogeneous cosmology which topic (or topics) are in your opinion the most important?}



\begin{figure}[h]
\begin{center}
\caption{{\em Within a domain of inhomogeneous cosmology which topic (or topics) are in your opinion the most important?}}
\includegraphics[scale=0.245]{question2.eps}
\label{q1-fig}
\end{center}
\end{figure}


\paragraph{Backreaction:}

Just under a half of responses pointed out that the most important topic of inhomogeneous cosmology is backreaction, and to understand how non-linearities and inhomogeneities affects the evolution of the universe and its properties.

A few pointed that the backreaction and averaging is not well defined and there are many various approaches to coarse--graining and therefore some unification scheme is important. 

\paragraph{Impact on observables:}

Slightly more than a third of respondents answered that the crucial element of inhomogeneous cosmology is to study light propagation effects in the inhomogeneous universe and the impact of inhomogeneities on observables.

\paragraph{Modelling the universe and structure formation:} 

27\% of respondents agreed that the most important topic is to develop a good models of the universe and structure formations. Within the domain of concordance cosmology this topic is studied using the perturbation approach or Newtonian N-body simulations. Most of the effort is put towards understanding baryonic feedback as it plays and important role in structure formation. However, inhomogeneous cosmology could provide an insight into how non-linear evolution and backreaction affects the formation of structures. 

Such studies are essential if we want to obtain a model capable of describing a universe at all scales with a required precision. This will also enable us to answer such questions as for example how accurate is current precision cosmology.

\paragraph{Testing the fundamental assumptions of the FLRW cosmology:}




\paragraph{Studying the exact inhomogeneous solutions of the Einstein Equations:}

\paragraph{Collaboration with observers}

If inhomogenosue cosmology becomes a tool of standard cosmology we need to engage in collaborations with observers who knows only standerd cosmology. Many of them are open but simply is not aware. In the intest oif inhomogenous cosmology it is collaboration with astornomers rather than separation from them.

See more on "


\paragraph{Early Universe:}

9\% of answers pointed out to studies of the early Universe (cf. 38\% of answers related to the early Universe in {\em Question 1}).


spikes 
BKL even if the early singularu was not genergcit



\subsection{Question 3:  How do you think the field of inhomogeneous cosmology will (or should) develop over the next 5--10 years?}



\begin{figure}[h]
\begin{center}
\caption{{\em How do you think the field of inhomogeneous cosmology will (or should) develop over the next 5--10 years?}}
\includegraphics[scale=0.245]{question3.eps}
\label{q1-fig}
\end{center}
\end{figure}


\paragraph{Developing a relativistic model of the Universe and structure formation:}



\paragraph{Deeper understanding of backreaction:}



\paragraph{Light propagation and developing tools to estimate the impact off structures on observables:}


\paragraph{Better use of wealth of available observational data:}


\paragraph{Developing new observational probes based on the insight from inhomogeneous cosmology:}



\paragraph{Continue testing the fundamental assumptions of the FLRW cosmology:}



\paragraph{Become part of the concordance cosmology:}


\section{The DE2 talks}

The topics outlined in the previous sections have been thoroughly discussed during the DE2 session of the 14th Marcel Grossmann Meeting. 
The session lasted 2 afternoons and involved 12 talks, lasting from 10 to 25 minutes. 
The talks can be roughly divided into three categories: focusing on exact results concerning the backreaction,
obrained within a known exact family of solutions (M. Korzy\'nski, T. Clifton, K. Bolejko), proposing various
more or less general approximate schemes, using a perturbative or post--Newtonian approach (V. A. Sanghai, J.-C. Hwang,
M. Eingorn, also J. Ostrowski with the discussion of the limitiations of the Green--Wald approach) and talks dealing with the
observational consequences of inhomogeneities directly (X. Yang) or theoretically using the light cone coordinates and observables (N. Bishop, H. Bester, F. Nugier, V. Reverdy).
We present here a short summary of each talk.

\paragraph{Mikołaj Korzyński, ``Nonlinear effects and coarse-graining in general relativity''.}
Coarse-graining is one of the fundamental ideas of physics, allowing to describe very complicated systems with a large number of degrees of freedom 
in an approximate but much simpler and tractable way. The basic idea is very simple: we carefully choose a number of variables describing the state of the system 
on large scale disregarding the fine details. We then derive the effective equations governing the behaviour of these collective, large-scale variables under certain reasonable 
assumptions about the physics of the fine scales and its influence on the large-scale variables. If the choice we have made is correct then the effective
equations form a closed system and we obtain a good approximation of the physics of the full system.

Coarse-graining is difficult in the context of nonlinear theories like general relativity, because the large--scale, effective stress--energy tensor, apart from the 
average of the local stress--energy tensor, will contain contibutions from the non--linear terms.
 The difference between the effective $T_{\mu\nu}$ and the naive average of the local one is often referred to as the backreaction \cite{Ellis:2011, Buchert:2015iva}.

The problem of estimating backreaction has recently been discussed extensively in the context of cosmology. The main question is whether or not we should take into account
the impact of the inhomogeneity of the matter distribution in the Universe on its large scale behaviour. 
The speaker discussed how the magnitude of the backreaction terms depends on the matter distribution on various scales and showed how clustering of
matter on small scales gives rise to nonlinear terms in effective, the coarse-grained quantities. 
Two distinct cases were considered: in the first one the matter content takes the form of a collection of very compact objects distributed in a homogeneous manner 
on large scales but not necessary
on small ones. In the second one the speaker considered a continuous distribution of dust in which the inhomogeneities have the form of a nested hierarchy of voids 
and overdense regons extending over
many scales. In both situations the solutions in question were simplified but exact and so were all the results.
The talk was based on the two recent papers: Refs.~\citenum{korzynski:2013tea, Korzynski:2014nna}. 




\paragraph{Timothy Clifton, ``What's The Matter In Cosmology?''}Almost all models of the universe start by assuming that matter fields can be modelled as dust. In the real universe,
however, matter is clumped into dense objects that are separated by regions of space that are almost empty. If we are to treat such a
distribution of matter as being modelled as a fluid, in some average or coarse-grained sense, then there is a number of questions that must be answered. 
One of the most fundamental of these is whether or not the interaction energy between masses should gravitate. If it does, then a dust-like description
may not be sufficient. We would then need to ask how interaction energies should be calculated in cosmology, and how they 
should appear in the Friedmann-like equations that govern the large-scale behaviour of the universe. The speaker discussed some recent results that may shed light on these questions. 

The talk was devoted to the interaction energies between massive bodies in cosmology. The questions the speaker
raised were (i) whether these interaction energies could be large in cosmology, and (ii) 
whether or not they should be expected to contribute to the RHS of the Friedmann-like 
equations that govern the large--scale expansion of space. Recent results seem to suggest that 
interaction energies can be large (although difficult to define), but they don't contribute much to 
the large--scale expansion when the bodies are separated by large distances. The talk was based on Refs.~\citenum{Clifton:2014mza, Clifton:2012qh}.


\paragraph{Viraj Sanghai, ``Post-Newtonian Cosmological Models''.} The talk was based on Ref.~\citenum{Sanghai:2015wia}. The speaker spoke about
a new approach developed together with T. Clifton 
 to study the effect of small scale non-linear structure on the large scale expansion 
of the universe. The authors used a bottom-up approach where they patched together weak field regions of the universe using the Israel junction conditions. 
The geometry of the weak field regions was described using a perturbed Minkowski metric and the matter distribution was described by the post-newtonian perturbative scheme.

In the case of a late time matter dominated universe, at Newtonian order, the authors obtained a standard Friedmann-like equation. At the post-Newtonian order, 
in the specific case of regularly arranged point masses, they obtained a radiation-like correction to this equation. This isn't an actual radiation. 
It appeared only due to the inhomogeneity of the model in question and due to the non-linearity of Einstein's field equations.

\paragraph{Jai-chan Hwang ``Fully nonlinear and exact perturbations of the Friedmann world model''.}
The speaker presented a joint work with H. Noh about a cosmological perturbation formulation valid to fully nonlinear order and its applications. 

\paragraph{Maksym Eingorn, ``Discrete cosmology: scalar perturbations in the conventional model and its extensions''.}
The mini-review talk “Discrete cosmology: scalar perturbations in the conventional model and its extensions”
was devoted to the gravitational field produced by separate inhomogeneities in the late Universe filled with 
nonrelativistic matter \cite{Eingorn:2012jm, Eingorn:2012dg, Eingorn:2013faa, Eingorn:2014hua, Eingorn:2014laa, Brilenkov:2014sda} and radiation
as well as additional perfect fluids with linear \cite{Burgazli:2013qy} or nonlinear \cite{Akarsu:2015wqa} equations of state, 
relic quark-gluon plasma \cite{Brilenkov:2013yva, Brilenkov:2013fka} or interacting dark sector \cite{Eingorn:2015rma}. Configurations of extra constituents 
dynamically coupled to pressureless
dust were at the center of attention. At present this limitation is removed, signifying construction of a promising perturbation method,
being valid for arbitrary scales, taking into account nonlinearity of fluctuations and responding to various physical challenges \cite{Eingorn:2015hza, Eingorn:2015yra}.

\paragraph{Xiaofeng Yang, ``Testing the cosmological principle of isotropy''.}
The speaker reviewed the test of the isotropic assumption of cosmological principle and introduced his recent work on the Type Ia
supernovae (SN Ia). The result indicates that the dipole fit is more sensitive than the hemisphere comparison method.
The main references of the talk are Refs.~\citenum{Yang:2013gea, PhysRevLett.99.081301, Gupta01092010, Gupta21072008, 0067-0049-208-2-19, Kalus:2012zu}. 

\paragraph{Krzysztof Bolejko, ``Exact Inhomogeneous Dust Solutions And Backreaction''. }

\paragraph{Nigel Bishop, ``The Characteristic Formalism: Cosmology Constructed from Observational Data''.}
The characteristic formalism uses outgoing null cones to define coordinates, and is well-developed in numerical relativity for
gravitational wave extraction. The formalism is well-suited to cosmology, since cosmological data are observed on past null cones.
In the case of spherical symmetry, density and radial velocity data on an initial null cone define a well-posed evolution problem leading 
to the past behaviour of the universe. Observationally, the data can be obtained from galactic number counts or age data, and the 
distance red-shift relation. When red-shift drift data becomes available, constraints on the theory of gravity, such as the cosmological 
constant, will become feasible.

The key references for the 
talk were: Refs.~\citenum{bish97, bish99} for the characteristic formulation of GR, Refs.~\citenum{bish96, vdWalt2010, vdWalt2012} on the construction of 
codes to evolve data from the past
null cone into its interior and Refs.~\citenum{Bester:2013fya, Bester:2015} on the use of real data to construct the geometry of the
past null cone and part of its interior.

\paragraph{Hertzog Bester, ``Towards the geometry of the universe from data''.}
The speaker gave an outline of an algorithm designed to reconstruct the background cosmological metric within the class of spherically symmetric dust universes that may include
a cosmological constant (otherwise known as $\Lambda$LTB models). It is build on the formalism developed in Refs.~\citenum{Bester:2013fya, Bester:2015} in which the aim 
is to reconstruct the geometry inside the
past lightcone of the observer directly from data. Gaussian process regression is used to
smooth certain observables on the current past lightcone which can then serve as priors
for a Markov-Chain-Monte-Carlo procedure. Luminosity and age data are used to derive
constraints on the geometry of the universe up to a redshift of z = 1.75. The propsed 
algorithm is meant to test for signs of inhomogeneities on large scales ($\ge$ 1 Gpc say).



\paragraph{Fabien Nugier, ``The geodesic light-cone coordinates, an adapted system for light-signal-based cosmology''.}
Most of cosmological observables are light-propagated quantities and thus make the use of null-like-signals adapted coordinates very relevant. 
The Geodesic Light Cone (GLC) system of coordinates is one of these by describing light propagation from a source to a geodesic observer in a 
trivial way. These coordinates, first defined in Ref.~\citenum{Gasperini:2011us}, are general and adapted to calculations in inhomogeneous geometries. 
They were applied to a large spectrum of topics including: light-cone averaging \cite{Gasperini:2011us}, the estimation of the distance-redshift 
relation in a perturbed FLRW spacetime \cite{BenDayan:2012wi,Marozzi:2014kua}, the corresponding contamination of the Hubble diagram 
\cite{BenDayan:2012ct,BenDayan:2013gc,Ben-Dayan:2014swa}, weak gravitational lensing \cite{Fanizza:2014baa,Fanizza:2015swa} and galaxy 
number counts \cite{DiDio:2014lka}. They were also recently applied to the propagation of ultra-relativistic particles \cite{Fanizza:2015gdn} 
and to describe the inhomogeneous Lema\^{i}tre-Tolman-Bondi \cite{Fanizza:2014baa} and anisotropic Bianchi I universes \cite{Fleury:2016htl}.


\paragraph{Vincent Reverdy, ``Light Propagation Through Cosmic Structures: Results From Cosmological Simulations'' .}
The anomaly of the apparent acceleration of expansion of the Universe requires going beyond the concordance paradigm of cosmology. 
In this context, the backreaction conjecture brings an elegant solution. But if we know that inhomogeneities have an effect on the local expansion rate,
the global amplitude of these effects remains largely unknown. Quantifying these effects will require inputs from theory, observations and numerical
simulations. In this talk the speaker presented results of relativistic raytracing in the weak-field approximation in cosmological simulations. Null geodesics
had been integrated up to the redshift of 30 for several virtual observers in the Dark Energy Universe Simulation: Full Universe Run. The speaked discussed how 
inhomogeneities affect the propagation of light on cosmological scales and how the angular diameter distance and the luminosity distance are affected by these effects. 
He also provided outlooks for the future of numerical simulations of inhomogeneous cosmologies aiming at quantifying backreaction effects in the long run. 

\paragraph{Jan Ostrowski, ``On the Green and Wald formalism''.}
Backreaction in the cosmological context is a longstanding problem that is
especially important in the present era of precise cosmology. The standard
model of a homogeneous background plus density perturbations is most
probably oversimplified and is expected to fail to fully account for the
near-future observations of sub-percent precision. From a theoretical
point of view, the problem of backreaction is very complicated and
deserves careful examination. Recently, Green and Wald claimed in a series
of papers to have developed a formalism to properly describe the influence
of density inhomogeneities on average properties of the Universe, i.e.,
the backreaction effect. A brief discussion of this framework was
presented, focussing on its drawbacks and on misconceptions that have
arisen during the ``backreaction debate''. It was based on Refs.~\citenum{Buchert:2015iva, 2015arXiv151202947O}.





\bibliographystyle{plain}
\bibliography{text}


\end{document}
